"use strict";
class A {
    constructor() {
        console.log(1);
    }
}
class B extends A {
    constructor() {
        super();
        console.log(2);
    }
}
const a5 = new B();
//# sourceMappingURL=index.js.map