let ddd = [true, true, false];
let e = {type: 'ficus'}
let f = [1, false]
const g = [3]
let h = null;


type T1 = {
    A: number;
    B: number;
}
type T2 = {
    C: number;
    D: number;
}

type T3 = T1 | T2;
type T4 = T1 & T2;

const a1: T3 = {
    A: 1,
    B: 2,
    C: 3,
    D: 4,
}

const a2: T4 = {
    A: 1,
    B: 3,
    C: 3,
    D: 4
}