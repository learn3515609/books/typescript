class A {
    protected constructor() {
        console.log(1)
    }
}

class B extends A {
    constructor() {
        super();
        console.log(2);
    }
}

const a5 = new B();


/////////////////


type Shoe = {
    purpose: string;
}

class BalletFlat implements Shoe {
    purpose = 'dancing';
}

class Boot implements Shoe {
    purpose = 'woodcutting';
}

class Sneaker implements Shoe {
    purpose = 'walking';
}

type ShoeClass = {
    create: {
        (type: 'balletFlat'): BalletFlat
        (type: 'boot' ): Boot
        (type: 'sneaker'): Sneaker
    }
}

let Shoe: ShoeClass = {
    create: (type: 'balletFlat' | 'boot' | 'sneaker') => {
        switch(type) {
            case 'balletFlat': return new BalletFlat;
            case 'boot': return new Boot;
            case 'sneaker': return new Sneaker;
        }
    }
}

const s51 = Shoe.create('balletFlat')
const s52 = Shoe.create('boot')
const s53 = Shoe.create('sneaker')

////////////////////

interface BuildableRequest {
    data: object
    method: 'get' | 'post'
    url: string
  }
  
class RequestBuilder2 {
    data?: object
    method?: 'get' | 'post'
    url?: string
  
    setData(data: object): this & {data: object} {
      return Object.assign(this, {data})
    }
  
    setMethod(method: 'get' | 'post'): this & {method: string} {
      return Object.assign(this, {method})
    }
  
    setURL(url: string): this & {url: string} {
      return Object.assign(this, {url})
    }
  
    build(this: BuildableRequest) {
      return this
    }
  }
  
  new RequestBuilder2()
    .setData({})
    .setMethod('post') // Try removing me!
    .setURL('bar') // Try removing me!
    .build()