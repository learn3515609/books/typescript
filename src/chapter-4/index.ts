type Reservation = string;

type Reserve = {
    (from: Date, to: Date, destination: string): Reservation
    (from: Date, destination: string): Reservation
    (destination: string): Reservation
}

let reserve: Reserve = (
    fromOrDestination: Date | string,
    toOrDestination?: Date | string,
    destination?: string
) => {
    if(typeof fromOrDestination === 'string') {
        return `Вылет сразу в ${fromOrDestination}`;
    } else if(typeof toOrDestination  === 'string') {
        return `Вылет ${fromOrDestination} в ${toOrDestination}`;
    } else {
        return `Вылет ${fromOrDestination} до ${toOrDestination} в ${destination}`;
    }
}

const r1 = reserve('Токио');
const r2 = reserve(new Date('2024-01-01'), 'Лондон');
const r3 = reserve(new Date('2024-01-01'), new Date('2024-12-01'), 'Москва');

console.log(r1);
console.log(r2);
console.log(r3);

function call<T extends [unknown, string, ...unknown[]], R>(
    f: (...args: T) => R,
    ...args: T
): R {
    return f(...args);
}

call(console.log, 1, 'string', 3)

function is<T>(...args: T[]) {
    for(let i = 0; i < args.length - 1; i++) {
        if(args[i] !== args[i + 1]) return false;
    }
    return true;
}

const is1 = is('string', 's')
const is2 = is(true, false)
const is3 = is(42, 42)
//const is4 = is(10, 's')
const is5 = is(1, 1, 1);

console.log(is1, is2, is3, is5)