type Banner = {
    title: string;
    version?: number;
    link?: string;
}

const render = (banner: Banner) => {
    console.log(banner)
}

render({
    title: 'Тест',
})

render({
    title: 'Тест',
    version: 5,
    link: 'https://'
})

render({
    title: 'Тест',
    version: 5,
    link: 'https://',
    llink: 'https://'
} as Banner)

render({
    title: 'Тест',
    version: 5,
    llink: 'https://'
} as Banner)

type Person = {
    name: string;
    children: {
        name: string;
        age: string;
    }[]
}

type Child = Person['children'][number]

// type Countries = Record<string, {x: number, y: number}>

type MyRecord<K extends string | number, T> = {
    [P in K]?: T
}

type Countries = MyRecord<string, {x: number, y: number}>

const countries: Countries = {
    russia: {x: 1000, y: 2000},
    russia2: {x: 1000, y: 2000},
    russia3: {x: 1000, y: 2000}
}

type Countries2 = MyRecord<'a' | 'b', {x: number, y: number}>

const countries2: Countries2 = {
    'a': {x: 1000, y: 2000}
}

type A6 = {
    a: number,
    b: number,
    c: number
}

type MyPartial<T> = {
    [K in keyof T]?: T[K]
}

type MyRequired<T> = {
    [K in keyof T]-?: T[K]
}

type MyPick<T, K extends keyof T> = {
    [P in K]: T[P]
}

let a6: MyPartial<A6> = {
    a: 1
}

let b6: MyRequired<MyPartial<A6>> = {
    a: 1,
    b: 2,
    c: 3
}

let c6: MyPick<A6, 'a' | 'b'> = {
    a: 1,
    b: 2
}


type AA6 = {
    a: number,
}

type BB6 = {
    a: string
}

const aa6 = {a: 1}
const bb6 = {a: '1'}

const isAA6 = (value: AA6 | BB6): value is AA6 => {
    return typeof value.a === 'number';
}

if(isAA6(bb6)) {
    console.log(bb6)
}

type Without<T, U> = T extends U ? never : T;

type ElementType<T> = T extends (infer U)[] ? U : T

type MyExclude<T, U> = T extends U ? never : T;
type A60 = number | string;
type B60 = string;
type C60 = MyExclude<A60, B60>

type MyExtract<T, U> = T extends U ? U : never;
type A61 = number | string;
type B61 = string;
type C61 = MyExtract<A61, B61>

type MyNonNullable<T> = T extends (null | undefined) ? never : T;
type A62 = {a?: number | null};
type B62 = MyNonNullable<A62['a']>

type MyReturnType<F> = F extends (...args: any) => infer R ? R : null;
type F63 = (a: number) => string;
type R63 = MyReturnType<F63>


// tasks

type Exclusive<T, U> = Exclude<T, U> | Exclude<U, T>;

type T64 =  Exclusive<1 | 2 | 3, 2 | 3 | 4>

let userId!: string;
fetchUser6();
userId.toUpperCase();
function fetchUser6() {
    userId = 'cache';
}